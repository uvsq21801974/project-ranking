package utils;

// ********************************************************************************************************************
// matrice creuse a deux dimensions (un tableau quoi)
//
// Seules les cellules de valeur non nuls sont representes.
// Ces cellules sont chainees entre eux via le lien next.
// head pointe sur la premiere cellule
// tail pointe sur la derniere de la liste
//
// lorsque les cellules sont rajoutées dans la matrice, on la chaine en fin de liste via la variable d'instance tail
// lorsque l'on veut parourir les cellules de la matrice creuse, le parcours commence par la vairable di'instance head
//
// aucun ordre particulier est exigé dans le chainage des cellules
// les cellules ne sont ni chainees en fonction de la ligne, ni en foncton de la colonne
// aucun ordre
// ********************************************************************************************************************


public class InMemorySparseMatrix  extends AbstractSparseMatrix {
			
    public  Entry  head;    // tete de liste des cellules non nulles
            Entry  tail;    // fin de liste des cellules non nulles


    
    public class Entry {
        public Entry  next; // next entry
        public int    r;    // row
        public int    c;    // column
        public double v;    // value, par construction v est non nulle
        
        Entry(int r, int c, double v) {
            this.r= r;
            this.c= c;
            this.v= v;
        }
    }   

	
    public InMemorySparseMatrix(String name, int nbLignes, int nbColonnes) {
        this.name= name;
        this.nbLignes= nbLignes;
        this.nbColonnes= nbColonnes;
    }
	
	
    // ***********************************************************************************************************
    //
	// Cette methode permet de parcourir toutes les cellules de la matrice pour les traiter une a une.
    //
    // ***********************************************************************************************************
    public void forEach(Consumer action) {
        for (Entry entry=head; entry!=null; entry= entry.next) {
            action.accept(entry.r, entry.c, entry.v);
        }
    }    
    // ***********************************************************************************************************
    //
    // rajoute une cellule dans la matrice
    // noter que:
    //      - si la valeur est nulle, aucune cellule est rajouté
    //      - aucun controle pour s'assurer qu'il n'y a pas de cellule déja presente à la laline r, et colonne c
    //        on remet ce controle a l'appelant
    //        Plus tard, modifier ce code, pour que le controle soit fait ici, et ne rien attendre de l'appelant.
    //
    // ***********************************************************************************************************
    public void add(int r, int c, double v) {		
        if (v==0)
            return;
        Entry entry= new Entry(r, c, v);
        if (head==null)
            head= entry;
        else
            tail.next= entry;
        tail= entry;	
    }

}
