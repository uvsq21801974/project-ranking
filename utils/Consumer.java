package utils;

@FunctionalInterface
public interface Consumer {
    void accept(int r, int c, double v);
}
