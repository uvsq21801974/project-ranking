package utils;

//
// inspired by the article from
//     https://www.geeksforgeeks.org/how-do-dynamic-arrays-work/
//
public class DynamicArray { 

	// create three variable array[] is a array, 
	// count will deal with no of element add by you and 
	// size will with size of array[] 
	private int array[]; 
	private int count; 
	private int size; 
	// constructor initialize value to variable 

	public DynamicArray() { 
		array = new int[1]; 
		count = 0; 
		size = 1; 
	} 
	// function add an element at the end of array 

	public void add(int data) { 

		// check no of element is equql to size of array 
		if (count == size) { 
			growSize(); // make array size double 
		} // insert element at end of array 
		array[count] = data; 
		count++; 
	} 

	// function makes size double of array 
	public void growSize() {
		int temp[] = null; 
		if (count == size) { 
			// temp is a double size array of array  and store array elements 
			temp = new int[size * 2]; 
    	    System.arraycopy(array, 0, temp, 0, size); // copy all array value into temp 
		} 

		// double size array temp initialize  into variable array again 
		array = temp; 
		
		// and make size is double also of array 
		size = size * 2; 
	} 

	public int getCount() {
		return count;
	}
	public int get(int i) {
		return array[i];
	}

} 
