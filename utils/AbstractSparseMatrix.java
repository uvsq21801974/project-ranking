package utils;

import utils.Consumer;

public abstract class AbstractSparseMatrix {
	public  String      name;    		// nom de la matrice
    public 	int    		nbLignes;    	// nombre de lignes dans la matrice
    public  int         nbColonnes;  	// nombre de colonnes dans la matrice

       
    // ***********************************************************************************************************
    // traiter toutes les cellules une par une.
    // ***********************************************************************************************************
     public abstract void forEach(Consumer action);

 
    
    // ***********************************************************************************************************
    // rajoute une cellule dans la matrice
    // ***********************************************************************************************************
    public abstract void add(int r, int c, double v);
    
    
    // ***********************************************************************************************************
    //
	// Cette methode permet d'afficher le contenu de la matrice.
	// La matrice étant certainement enorme, on n'affichera tout au plus 10 lignes sur 10 colonnes
    //
    // ***********************************************************************************************************
    public void affiche(){
        int n= Math.min(10, nbLignes);
        int m= Math.min(10, nbColonnes);
       
        double[][] M= new double [n][];
        for (int i=0; i<n; i++) {
            M[i]= new double[m];
        }
        
        this.forEach((r,c,v) -> {
                                if (r<n && c<m) {
                                    M[r][c]= v;
                                }
                             }
                    );

        System.out.printf("%s =\n", name);
        StringBuilder sb= new StringBuilder(128);
        sb.append("    +");
        for (int i=0; i<m; i++)
            sb.append("------------+");
        sb.append('\n');
        String footer= sb.toString();

        for (int r=0; r<n; r++) {
            sb.append("    | ");
            for (int c=0; c<m; c++) {
                if (c>0)
                    sb.append(" | ");
                double v= M[r][c];
                if (v==0)
                    sb.append("          ");
                else
                    sb.append(String.format("%.8f", v));
            }
            sb.append(" |\n");
        }
        sb.append(footer);
        System.out.print(sb.toString());
    }
}
