package utils;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.LineNumberReader;

import java.util.Arrays;

public class Matrix {
    
    // **********************************************************
    // G est une matrice carree
    //                               +-     -+
    //                               | 1 0 1 |
    // result = p x G = [ 1  2  3] x | 0 1 2 |
    //                               | 2 0 3 |
    //                               +-     -+
    // **********************************************************
    public static double[] mult(double[] p, double[][] G){
//      System.out.println("Matrix.mult()");
        int n = p.length;
//      System.out.printf("    p[3]=%f\n", p[3]);
        
        double[] result= new double[n];
        for (int i=0; i<n; i++) {
            double v= 0;
            for (int k=0; k<n; k++) {
                v= v+p[k]*G[k][i];
//              if (i==0 && k==3 || i==3) {
//                  System.out.printf("    i=%d, k=%d, p[k]=%f, G[k][i]=%f, v=%f\n", i, k, p[k], G[k][i], v);
//              }
            }
            result[i]= v;
//          if (i==0)
//              System.out.printf("    result[0]=%f\n", v);
//          if (i==3)
//              System.out.printf("    result[3]=%f\n", v);
        }
        return result;
    }
    
    // **********************************************************
    //                                      +- -+
    //                                      | 1 |
    // result = p x f = [ 0.1  0.2  0.73] x | 0 |
    //                                      | 0 |
    //                                      +- -+
    // **********************************************************
    public static double mult(double[] p, double[] f){
        int n = p.length;
        double result= 0;
        for (int i=0; i<n; i++) {
            result= result + p[i]*f[i];
        }
        return result;
    }    
    // **********************************************************
    // M peut ne pas etre une matrice carree
    //     +-       -+
    //     | 1 0 1 4 |
    // M = | 0 1 2 0 |
    //     | 2 0 3 2 |
    //     +-       -+
    // **********************************************************
    public static void affiche(String label, double[][] M){
        int n1= M.length;
        int m1= M[0].length;
        
        int n2= Math.min(10, n1);
        int m2= Math.min(10, m1);
        System.out.printf("%s =\n", label);
        StringBuilder sb= new StringBuilder(128);
        sb.append("    +");
        for (int i=0; i<m2; i++)
            sb.append("------------+");
        sb.append('\n');
        String footer= sb.toString();

        for (int r=0; r<n2; r++) {
            sb.append("    | ");
            for (int c=0; c<m2; c++) {
                if (c>0)
                    sb.append(" | ");
                double v= M[r][c];
                if (v==0)
                    sb.append("          ");
                else
                    sb.append(String.format("%.8f", v));
            }
            sb.append(" |\n");
        }
        sb.append(footer);
        System.out.print(sb.toString());
    }
    
    public static void affiche(String label, int[] M) {
        int n= M.length;
        StringBuilder sb= new StringBuilder(64);
        sb.append(label).append(" = [ ");
        int i=0;
        for (int v:M) {
            sb.append(String.format("%d ", v));
            i++;
            if (i>=10) {
                sb.append(" ... ");
                break;
            }
        }
        sb.append("]");
        if (n>10)
            sb.append(String.format(" (size = %d)", n));
        System.out.printf("%s\n", sb.toString());
    }
    
    public static void affiche(String label, double[] M) {
        int n= M.length;
        StringBuilder sb= new StringBuilder(64);
        sb.append(label).append(" = [ ");
        int i=0;
        for (double v:M) {
            sb.append(String.format("%.8f ", v));
            i++;
            if (i>=10) {
                sb.append(" ... ");
                break;
            }
        }
        sb.append("]");
        if (n>10)
            sb.append(String.format(" (size = %d)", n));
        System.out.printf("%s\n", sb.toString());
    }
    
    
    public static double[] reorganise(double[] v, int[] map) {
    	int n = v.length;
        double[] vtrie = new double[n];
        for (int i = 0 ; i < n ; i++) 
        	vtrie[map[i]] = v[i];
        return vtrie;
    }
    
    public static double differenceNorme1(double[] v1, double[] v2) {
    	double max = 0;
    	for (int i = 0 ; i < v1.length ; i++) {
    		double dif = Math.abs(v1[i] - v2[i]);
    		if (dif > max) 
    			max = dif;
    	}
    	return max;
    }
}
