package utils;

public class PartitionResult {
    public FileBasedSparseMatrix matrix;
    public int[]                 partition;

    public PartitionResult(FileBasedSparseMatrix matrix, int[] partition) {
        this.matrix= matrix;
        this.partition= partition;
    }
}
