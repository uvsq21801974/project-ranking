package utils;

import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;



import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;


// ********************************************************************************************************************
// matrice creuse (et carree)
//
// Seules les cellules de valeur non nuls sont representes.
// Les cellules resident sur disque dans un fichier dedie à la matrice.
// A la matrice de nom G sera associe deux fichiers:
//        1) le fichier G.data qui sera destine à recvevoir toutes les cellules da la matrice
//        2) le fichier G.dd (dd comme data descriptor) qui contient les méta data
//           pour l'instant comme meta data, on y stocke la dimension de la matrice.
// Les cellules ne prennent pas de place sur la RAM.
// Les cellules lorsqu'il sont ressucitées sur le heap java, le seront que le temps limité, le temps de son traitement
// dans foreach.
// Et ainsi épargner l'utilisation de la mémoire RAM.
//
// ********************************************************************************************************************


public class FileBasedSparseMatrix extends AbstractSparseMatrix {
	//tableau de permutation
    public int[] map;
    private DataOutputStream    dos;
       
    // ***********************************************************************************************************
    //
    // Cette methode permet de parcourir toutes les cellules de la matrice pour les traiter une a une.
    //
    // ***********************************************************************************************************
     public void forEach(Consumer action) {
        DataInputStream din= null;
        int since=0;
        String ifnm= name+".data";
        File file= new File(ifnm);
        if (!file.exists()) {
            System.out.printf("file '%s' does not exist\n", ifnm);
            return;
        }
        try {
            din = new DataInputStream(new BufferedInputStream(new FileInputStream(ifnm)));
            int count=0;
            long t1= System.currentTimeMillis();
            for(;;) {
                long t2= System.currentTimeMillis();
                long delta= t2-t1;
                if (delta>=60000) {
                    since++;
                    System.out.printf("%2d mn\t%8d\n", since, count);
                    t1= t2;
                }
                count++;

                int r= din.readInt();
                int c= din.readInt();
                double v = din.readDouble();
                
                action.accept(r, c, v);
            }
        }
        catch(EOFException eof) {
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
        finally {
            try {din.close();} catch(Exception ex){}
        }
    }

    public FileBasedSparseMatrix(String name) {
        this.name= name;
        
        String ifnm= name+".dd";
        File file= new File(ifnm);
        if (file.exists()) {
            DataInputStream din= null;
            try {
                din = new DataInputStream(new FileInputStream(ifnm));
                nbLignes= nbColonnes= din.readInt();
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
            finally {
                try {din.close();} catch(Exception ex){}
            }
        }
    }

    public FileBasedSparseMatrix(String name, int nbLignes) {
        this.name= name;
        this.nbLignes= this.nbColonnes= nbLignes;
		
        try {
            String dfnm= name+".dd";
            dos = new DataOutputStream(new FileOutputStream(dfnm));
            dos.writeInt(nbLignes);
            dos.close();
        }
        catch(Exception ex) {ex.printStackTrace();}
    }
    
    // ***********************************************************************************************************
    //
    // rajoute une cellule dans la matrice
    // noter que:
    //      - si la valeur est nulle, aucune cellule est rajouté
    //      - aucun controle pour s'assurer qu'il n'y a pas de cellule déja presente à la laline r, et colonne c
    //        on remet ce controle a l'appelant
    //      - la rajout d'une cellule consiste à ecrirer sur disque à la fin du fichier les informations relatives
    //        a la cellule que l'on vient de rajouter
    //
    // ***********************************************************************************************************
    public void startPopulate() {
        String ofnm= name+".data";
        try {
            dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ofnm)));
        }
        catch(Exception ex) {ex.printStackTrace();}
    }
    public void add(int r, int c, double v) {
        if (v==0)
            return;
        try {
            dos.writeInt(r);
            dos.writeInt(c);
            dos.writeDouble(v);
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
    public void stopPopulate() {
        try {dos.close();} catch(Exception ex) {ex.printStackTrace();}
        dos= null;
    }
    
    // **********************************************************
    // G est une matrice carree
    //                                  +-     -+
    //                                  | 1 0 1 |
    // result = p x this = [ 1  2  3] x | 0 1 2 |
    //                                  | 2 0 3 |
    //                                  +-     -+
    // **********************************************************
    public double[] mult(double[] p){
        if(nbLignes != p.length) {
            System.err.printf("p is an array of size %d (while %d is expected!)\n", p.length, nbLignes);
            System.exit(1);
        }
        double[] result= new double[nbLignes];
        this.forEach((r, c, v) -> result[c] += p[r]*v );

        return result;
    }
    
    
    // **********************************************************
    // Les fichiers de graphes se lisent comme ceci
    //   * ----------    
    //   | nombre de pages dans le graphe
    //   | nombre de liens dans le graphe
    //   | numero du premier sommet, degré sortant, successeur, poids du lien, successeur 2, poids du lien...
    //   | numero d'un autre sommet, degré sortant, successeur, poids du lien, successeur 2, poids du lien...
    //   :
    //   :
    //  exemple pour la matrice     +-              -+
    //                              | 0.2   0    0.8 |
    //                              | 0     1.0  0   |
    //                              | 0.5   0.4  0.1 |
    //                              +-              -+
    //   * --------------------    
    //   |3
    //   |6
    //   |1 2 1 0.2 3 0.8
    //   |2 1 2 1.0
    //   |3 3 2 0.4 3 0.1 1 0.5
    //   * -------------------- 
    // **********************************************************
    public void lire(String fname) {
        try {
            System.out.println("");
            System.out.printf("lire2('%s')\n", fname);
            
            
            Scanner input = new Scanner(new File(fname));
            int n = Integer.parseInt(input.nextLine().trim());
            System.out.printf("1. n=%d\n", n);
            this.nbLignes = n;
            this.nbColonnes = n;
            
            {
                String dfnm= name+".dd";
                dos = new DataOutputStream(new FileOutputStream(dfnm));
                dos.writeInt(n);
                dos.close();
            }
            
            startPopulate();

            input.nextLine();
            
            long t0= System.currentTimeMillis();
            int i=0;
            for (int page = 0 ; page < n ; page++ ) {
                long t1= System.currentTimeMillis();
                if (t1-t0>=10000) {
                    System.out.printf("%3d s\tpage= %8d\n", i*10, page);
                    i++;
                    t0= t1;
                }
                input.useDelimiter("\\s+");
                String somm = input.next().replace("\n", "");
                int sommet = Integer.parseInt(somm);
                int degreSortant = Integer.parseInt(input.next());
                for (int succ = 0 ; succ < degreSortant - 1 ; succ++ ) {
                    String next = input.next();
                    int fils = Integer.parseInt(next);
                    next = input.next();
                    double poids = Double.parseDouble(next);
                    add(sommet - 1, fils - 1, poids);
                }
                if (degreSortant > 0) {
                    //on gère le retour à la ligne en fin de ligne
                    String next = input.next();
                    int fils = Integer.parseInt(next);
                    input.useDelimiter("\n");
                    next = input.next();
                    double poids = Double.parseDouble(next);
                    add(sommet - 1, fils - 1, poids);
               }
            }
        } 
        catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
            stopPopulate();
        }
    }  
    
    int arcsfiltres = 0;
    public LinkedHashSet<DynamicArray> calculComposantesConnexes(double epsilone) {
    	LinkedHashSet<DynamicArray> composants= new LinkedHashSet();
    	 
	    final DynamicArray[] i2composant= new DynamicArray[nbLignes];
	    {
	        for (int i=0; i<nbLignes; i++) {
	            DynamicArray composant= new DynamicArray();
	            composant.add(i);
	            i2composant[i]= composant;
	        }
	        
	        this.forEach((r, c, v) -> {
	                                if (v<epsilone) {
	                                	arcsfiltres++;
	                                    return;
	                                }
	                                DynamicArray composant1= i2composant[r];
	                                DynamicArray composant2= i2composant[c];
									if (composant1==composant2)
										return;
	
	                                // ------------------------------------------
	                                // let's merge the two sets
	                                // ------------------------------------------
									int count1= composant1.getCount();
									int count2= composant2.getCount();
									if (count1 >= count2) {
										//     composant1= composant1 + composant2
										for (int i2=0; i2<count2; i2++) {
											int elt2= composant2.get(i2);
											composant1.add(elt2);
											i2composant[elt2]= composant1;
										}
									}
									else {
										//     composant2= composant1 + composant2
										for (int i1=0; i1<count1; i1++) {
											int elt1= composant1.get(i1);
											composant2.add(elt1);
											i2composant[elt1]= composant2;
										}
									}
	                              }
	                    );          
	
	        for (int i=0; i<nbLignes; i++) {
	            DynamicArray composant= i2composant[i];
	            composants.add(composant);
	        }
	        
	    }
	    System.out.println("nb partitions : "+composants.size());
	    System.out.println("nb arcs filtres : "+ arcsfiltres);
	    return composants;
	 
    }
	
    // ***************************************************************************************************************
    // Notes: 
    // 1)  on n'a pas besoin de cosntruire explicitement la matrice Mepsilon
    //     C'est une optimisation importante: il nous évite de parcourir inutilement la matrice d'entrée qui est énorme
    // 2)  le composant est un tableau d'entiers ordonné par ordre croissante
    // ***************************************************************************************************************
    public PartitionResult partitionne(double epsilone) {
		System.out.println("");
		System.out.printf("partitionne(epsilone=%f)\n", epsilone);
        int[] partition;

        
        String ifnm= name+".data";
        
        // --------------------------------------
        // identifier les composants connexes
        // --------------------------------------
        
        LinkedHashSet<DynamicArray> composants = this.calculComposantesConnexes(epsilone);
        
        // ---------------------------------------
        // affichons les differents composants
        // on affiche au maximum 10 composants
        // ---------------------------------------
        {
            int i=0;
            for (DynamicArray composant:composants) {
                if (i>=100)
                    break;
                StringBuilder sb= new StringBuilder();
                sb.append("{ ");
                int j= 0;
				int count= composant.getCount();
                for (int k=0; k<count; k++) {
					int v= composant.get(k);
                    sb.append(String.format("%d ", v));
                    j++;
                    if (j>10) {
                        sb.append(" ... ");
                        break;
                    }
                }
                sb.append("}");
                if (composant.getCount()>10)
                    sb.append(String.format(" (size = %d)", composant.getCount()));
                System.out.println(sb.toString());
                i++;
            }
        }
        System.out.println("");
        
        // ----------------------------------------------------------------------------
        // initialiser la variable partitions qui donne la taille de chaque composant
        // ----------------------------------------------------------------------------
        {
            int nbComposants= composants.size();
            partition= new int[nbComposants];
            int i=0;
            for (DynamicArray composant: composants)
                partition[i++]= composant.getCount();
        }
        
        Matrix.affiche("partition", partition);
        
        // ---------------------------------------
        // generons G
        // ---------------------------------------
        FileBasedSparseMatrix G= new FileBasedSparseMatrix("G");
        G.nbLignes= nbLignes;
        try {
            String dfnm= "G.dd";
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(dfnm));
            dos.writeInt(nbLignes);
            dos.close();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        
        // map
        map= new int[nbLignes];
        {
            int i=0;
            for (DynamicArray composant: composants) {
				int count= composant.getCount();
                for (int j=0; j<count; j++) {
                    map[i++]= composant.get(j);
                }
            }
        }
        Matrix.affiche("map", map);
        
         // revMap est fonction inverse de map
         // revMap o map = I
        int[] revMap= new int[nbLignes];
        for (int i=0; i<nbLignes; i++)
            revMap[map[i]]= i;
        Matrix.affiche("revMap", revMap);
       
        System.out.println("");
        System.out.println("start constructing G");
        System.out.println("--------------------");
        // generons G
        //      (r, c) = (row, column) indexe M
        //      (i, j) indexe G
        {
            G.startPopulate();
        
            this.forEach((r, c, v) -> G.add(revMap[r], revMap[c], v) );


            G.stopPopulate();
//          G.affiche();
        }
        return new PartitionResult(G, partition);
    }
}
