package com.sp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import utils.Matrix;
import utils.InMemorySparseMatrix;
import utils.FileBasedSparseMatrix;
import utils.PartitionResult;

// ********************************************************************************************************************
// Description des variables globales
//     M         represente la matrice de depart dont on doit appliquer l'approche KMS simplifiee
//     G         represente la matrice resultante du partitionnement en composants connexes de M 
//     P         rassemble toutes les matrices Pij
//     partition (nous cherchons encore un nom qui soit plus representatif de cette variable)
//               cette variable nous donne la taille des differents composants connexes de G
//               Ainsi partition = [3, 2] signifie que la partition est constituee de deux composants connexes
//               dont le premier composant est de taille 3, le deuxieme composant est de taille 2
//     A         represente la matrice de couplage
//     beta      represente le vecteur de probabilites (solution de beta x A = beta) obtenu par la methode des puissances
//     alfaK
//     piK
//
// Les matrices M, G et A sont representees par des matrices creuses dont le contenu est persiste sur disque.
//      Meme creuses, elles sont énormes pour que leur contenu puisse etre gardee en mémoire RAM (heap java).
//      C'est pourquoi, leur contenu sont sur disque. N'est gardée en mémoire les cellules le temps d'un calcul.
//      Elles sont de classe FileBasedSparseMatrix
// Les matrices Pij sont également de matrices creuse.
//      Pour l'instant, on part de l'hypothese que leur contenu puisse etre entirement en mémoire RAM (heap java)
//      Elles sont de classe InMemorySparseMatrix.
//      Si par la suite, l'hypothèse s'avère fausse, on les declarera comme classe FileBasedSparseMatrix.
//
// pseudo code
//     remplir la matrice M a partir d'un fichier                               -> M
//     partitionne la matrice M selon les composants connexes                   -> G, partition
//     initialiser alfaK
//     initialiser piK
//     a partir de G construire P (autrement dit, on construit tous les Pij)    -> P
//     initialiser pi
//     on boucle jusqu'à convergence
//          en fonction de pi calculer la matrice de couplage                   -> A
//          calculer vecteur de probabilite de A                                -> beta
//          calculer alfaK                                                      -> alfaK
//          calculer piK                                                        -> piK
//          calculer pi
//          comparer piK avec sa precedente valeur comme test de convergence
// ********************************************************************************************************************
public class Main {
	static class CompositeKey {
		int i; 
		int j;
		
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			CompositeKey theOther= (CompositeKey)obj;
			return theOther.i==i && theOther.j==j;
		}
        public int hashCode() {
            return i<<16 | j;
        }
		CompositeKey() {
		}
		CompositeKey(int i, int j) {
			this.i= i;
			this.j= j;
		}
	};
	
    static FileBasedSparseMatrix M;
    
    static FileBasedSparseMatrix G;
    static double[] f;                              // f associee à la matrice G
    static int[] partition;                         // donne la taille de chacun des composants connexes.  (le choix du nom de la variable n'est pas pertinent
	static int[] cumuls;
                       
    static Map<CompositeKey, InMemorySparseMatrix> P;     // permet à partir de la cle "Pi,j" retrouver le Pij associé
                           
    // *************************************
    //      pi = (pi1, pi2)
    // *************************************
    static double[][] pi;

    
    // *************************************
    //        matrice de couplage A
    // *************************************
    static FileBasedSparseMatrix A;

    // *************************************
    //        vecteur de probabilite
    // *************************************
    static double[] beta;

    // *************************************
    //        alfa(k)
    // *************************************
    static double[] alfaK;
    
    // *************************************
    //        pi(k)
    // *************************************
    static double[] piK;
    
    
    // **************************************************************************
    // affiche P
    // --------------------------------------------------------------------------
    // Trop d'informations tue l'information
    // On n'affiche que les Pij des 10 premieres lignes et 10 premieres colonnes 
    // **************************************************************************
    public static void afficheP() {
		CompositeKey key= new CompositeKey();
        int n= partition.length;
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                key.i= i; key.j= j;
                InMemorySparseMatrix Pij= P.get(key);
                if (Pij==null)
                    continue;

                Pij.affiche();
            }
        }
    }
    
    // **************************************************************************
    // affiche Pi
    // --------------------------------------------------------------------------
    // Trop d'informations tue l'information
    // On n'affiche que les 10 premieres valeurs de pi 
    // **************************************************************************
    public static void affichePi() {
        StringBuilder sb= new StringBuilder("pi = [ ");
        int i=0;
loop:
        for (double[] pi_i:pi) {
            for (double v:pi_i) {
                sb.append(String.format("%.8f ", v));
                i++;
                if (i>10) {
                    sb.append(" ...");
                    break loop;
                }
            }
        }
        sb.append("]");
        if (G.nbLignes>10)
            sb.append(String.format(" (nbLignes = %d)", G.nbLignes));
        System.out.printf("%s\n", sb.toString());
    }
    
    public static void initialiserAlfaK() {
        int n= G.nbLignes;
        alfaK= new double[n];
        for (int i=0; i<n; i++)
            alfaK[i]= 0;
    }
    public static void initialiserPiK() {
        int n= G.nbLignes;
        piK= new double[n];
        for (int i=0; i<n; i++)
            piK[i]= 0;
    }

    public static void initialiserPi() {
        int n= partition.length;
        pi= new double[n][];
        for (int i=0; i<n; i++) {
            int segmentSize= partition[i];
            pi[i]= new double[segmentSize];
            for (int j=0; j<segmentSize; j++)
                pi[i][j]= ((double)1)/segmentSize;
        }
    }
    // ***********************************************************************************************************
    // retoune le numero de composant d'une indice
    // -----------------------------------------------------------------------------------------------------------
    // imaginons que 
    //     - la partition est composee de 2 composants
    //     - le premier composant est de taille 3
    //     - le deuxieme composant est de taille 2
    // alors
    //         0 -> 0
    //         1 -> 0
    //         2 -> 0
    //         3 -> 1
    //         4 -> 1
    // qui signifie que les 3 premiers sont associes au premier composant
    //                  les 2 derniers sont associes au deuxime composant
    // sachant qu'en java le premier c'est 0, le deuxieme c'est 1 ...
    // ***********************************************************************************************************
    public static int getComposant(int i) {
        for (int k=0; k<cumuls.length; k++) {
            int cumul=cumuls[k];
            if (i<cumul)
                return k;
        }
        return -1;
    }    
   
    // ***********************************************************************************************************
    // retoune l'indice dans le composant
    // -----------------------------------------------------------------------------------------------------------
    // imaginons que 
    //     - la partition est composee de 2 composants
    //     - le premier composant est de taille 3
    //     - le deuxieme composant est de taille 2
    // alors
    //         0 -> 0
    //         1 -> 1
    //         2 -> 2
    //         3 -> 0
    //         4 -> 1
    // En effet 3 premiers sont associes au premier composant, et prennent 
    // successivement les valeurs 0, 1 et 2 dans le composant
    //      les 2 derniers sont associes au deuxieme composant, et prennnet 
    // succesibvement les valeurs 0, 1 dans le composant
    // sachant qu'en java le premier c'est 0, le deuxieme c'est 1 ...
    // ***********************************************************************************************************
    public static int getIndiceDansComposant(int composant, int i) {
		if (composant==0)
			return i;
		else
			return i-cumuls[composant-1];
	}
	
    // ***********************************************************************************************************
    // construire P a partir de G
    // -----------------------------------------------------------------------------------------------------------
    // P associe une cle "Pij" le tableau Pij
    // ***********************************************************************************************************
    public static void construireP() {
        System.out.println("");
        System.out.println("construireP()");
        System.out.println("=============");
        int n= partition.length;
        System.out.printf("n= %d\n", n);
        P= new HashMap();
        
		CompositeKey key= new CompositeKey();        
		StringBuilder sb= new StringBuilder();
        G.forEach((r, c, v) -> {
                                int i=getComposant(r);
                                int j=getComposant(c);
								
                                // indice de l'element dans le composant
                                int x= getIndiceDansComposant(i,r); 
                                int y= getIndiceDansComposant(j,c); ;
                                
								key.i= i; key.j= j;
                                InMemorySparseMatrix Pij= P.get(key);
                                if (Pij==null) {
                                    int size1= partition[i];
                                    int size2= partition[j];
									sb.setLength(0);
									String nm= sb.append('P').append(i).append(',').append(j).toString();
                                    Pij= new InMemorySparseMatrix(nm, size1, size2);
                                    P.put(new CompositeKey(i, j), Pij);
                                }
                                Pij.add(x,y,v);
                            }
                    );
        
        
        //
        // pour nous donner une idéee a tel point la matrice est creuse, on affiche le taux d'occupation de la table
        //
        {
            int size= P.size();
            long nbTotalCases= n*n;
            long nbCasesVides= nbTotalCases - size;
            
            System.out.printf("nbCasesVides = %d sur %d (soit %d %%)\n", nbCasesVides, nbTotalCases, (int)((((double)nbCasesVides)*100)/nbTotalCases));
            System.out.println("");
        }
    }

    
    // ***********************************************************************************************************
    // calcul de la matrice de couplage a partir de A
    // -----------------------------------------------------------------------------------------------------------
    // ***********************************************************************************************************
    private static double __valeur;
    public static FileBasedSparseMatrix calculMatriceDeCouplage(double[] f) {  
        int n= partition.length;
        for (int i=0; i<n; i++) {
            f[i]= 1;
        }

        FileBasedSparseMatrix A= new FileBasedSparseMatrix("A", n);
        A.startPopulate();
        for (CompositeKey key:P.keySet()) {
            InMemorySparseMatrix Pij= P.get(key);
            
            int i= key.i;
            int j= key.j;
            
            double[] pi_i= pi[i];
            
            __valeur= 0;
            Pij.forEach((r, c, v) -> __valeur += v*pi_i[r] );

            if (__valeur!=0) {
                A.add(i, j, __valeur);
                f[i]= 0;
            }
        }
        A.stopPopulate();
        return A;
    }

    // ***********************************************************************************************************
    // On utilise la methode des puissances pour calculer le vecteur de probabilites
    // En entree:
    //     les variables globaux 
    //         double[][] A
    //         double[] partition
    // En sortie
    //     retourne le vecteur de probabilité    
    // A Faire:
    //     Il faut permettre de variabiliser 
    //         alfa
    //         iteration
    // ***********************************************************************************************************
    public static double[] calculVecteurDeProbabilite(double[] f, double alfa) {
        int n=partition.length;
               
        double[] p= new double[n];
        for (int i=0; i<n; i++)
            p[i]= ((double)1)/n;
        double epsilon=10E-10;

        double correction2= ((double)1-alfa)/n;
        
        double[] prevP= p;
        for (int iteration=1; iteration<=1000; iteration++) {
            p= A.mult(prevP);           
            double correction1= Matrix.mult(prevP, f)/n;
            for (int i=0; i<n; i++) {
                p[i]= alfa*p[i]+correction1+correction2;
            }
            double ecart=0;
            for (int i=0; i<n; i++) {
                double delta= Math.abs(p[i]-prevP[i]);
                ecart= Math.max(ecart, delta);
            }
            if (ecart<=epsilon)
                break;
            prevP= p;
        }

        return p;
    }    

    public static void calculAlfaK() {      
        int alfaK_x=0;
        int beta_x= 0;
        for (double[] pi_i:pi) {
            for (double v:pi_i) {
                alfaK[alfaK_x++]= beta[beta_x]*v;
            }
            beta_x++;
        }
    }
    public static void calculPi() {
        int compX=0;
        int piK_x=0;
        for (double[] pi_i:pi) {
            int nc= pi_i.length;
            double s=0;
            for (int c=0; c<nc; c++) {
                s= s+piK[piK_x+c];
            }
            
            // ------------------------------------------------------------------------------------------
            // par precaution, on rajoute ce test, pour s'addurer que le denominateur ne peut etre nulle
            // mais a vrai dire il (s) ne peut etre 0
            // compte tenu de la construction de piK
            // ------------------------------------------------------------------------------------------
            if (s==0) {
                for (int c=0; c<nc; c++) {
                    pi_i[c]= 0;
                }
            }
            else {
                for (int c=0; c<nc; c++) {
                    pi_i[c]= piK[piK_x+c]/s;
                }
            }
            piK_x+=nc;
            compX++;
        }
    }
    
    public static void calculF() {
        int n=G.nbLignes;
        f= new double[n];
        for (int i=0; i<n; i++) {
            f[i]= 1;
        }
        G.forEach((r, c, v) -> {
                                if (v==0)
                                    return;
                                f[r]=0;
                            }
                  );
    }
    public static void calculPiK() {
        if (f==null)
            calculF();
        int n=G.nbLignes;
        

        double alfa= 1;
        double correction2= ((double)1-alfa)/n;
        
        double[] prevAlfaK= alfaK;
        alfaK= G.mult(prevAlfaK);           
        double correction1= Matrix.mult(prevAlfaK, f)/n;
        for (int i=0; i<n; i++) {
            alfaK[i]= alfa*alfaK[i]+correction1+correction2;
        }
        piK= alfaK;
    }
	
	
    public static void main(String[] args) throws Exception {
        
        double epsilone= 0.0001;
        //convergence
        double e=10E-8;
        double alpha = 0.9;
        
        //if (args.length==1)
            //fileName= args[0];
		String fileName= "Stanford_BerkeleyV2.txt";
        System.out.printf("fileName= '%s' epsilon=%f\n", fileName, epsilone);
             
		  
        M= new FileBasedSparseMatrix("M");
		M.lire("data/"+fileName);
        M.affiche();

        
//        M.calculComposantesConnexes(epsilone);
//        if (true) {
//        	return;
//        }
        
		PartitionResult result= M.partitionne(epsilone);
		G= result.matrix;
		partition= result.partition;
		
        // ------------------------------------------
		// extra step: precompute cumuls
        // ------------------------------------------
		{
			cumuls= new int[partition.length];
			int cumul= 0;
			for (int i=0; i<partition.length; i++) {
				cumuls[i]= cumul= cumul+partition[i];
			}
		}
		Matrix.affiche("cumuls", cumuls);
			

        long debut = System.currentTimeMillis();

        initialiserAlfaK();
        initialiserPiK();
		long debut1 = System.currentTimeMillis();
        construireP();
		long fin1 = System.currentTimeMillis();
		System.out.println("Temps contruction de p: " + (fin1 - debut1)/(double) 1000);

//      afficheP();
        
        // ------------------------------------------
        // 1. Initialiser pi
        // ------------------------------------------
        initialiserPi();
        affichePi();
		File f = new File("results/ecart" + fileName);
		if (f.exists()) {
			f.delete();
		}
		f.createNewFile();
		PrintStream writerEcart = new PrintStream(f);


        double[] prevPiK= null;
        for (int iteration=1; ;iteration++) {
            {
                double[] fDeA= new double[partition.length];
                // ------------------------------------------
                // 2. calcul du matrice de couplage A
                // ------------------------------------------
                A= calculMatriceDeCouplage(fDeA);
                
                // ------------------------------------------
                // 3. calcul du vecteur de probabilite beta
                // ------------------------------------------
                beta= calculVecteurDeProbabilite(fDeA, alpha);
                fDeA= null;
//              Matrix.affiche("beta", beta);
            }
            
            // ------------------------------------------
            // 4. calcul de alfa(k)
            // ------------------------------------------
            calculAlfaK();
//          Matrix.affiche("alfaK", alfaK);
            
            // ------------------------------------------
            // 5. calcul de pi(k)
            // ------------------------------------------
            calculPiK();
//          Matrix.affiche("piK", piK);
            
            // ------------------------------------------
            // 6. calcul de pi
            // ------------------------------------------
            calculPi();

            // ------------------------------------------
            //    test de convergence
            // ------------------------------------------  
            if (iteration==1) 
                System.out.println("n\tecart");
            else {
                double ecart= 0;
                for (int j=0; j<piK.length; j++) {
                    ecart= Math.max(ecart, Math.abs(piK[j] - prevPiK[j]));
                }
                System.out.printf("%3d\t%.12f\n", iteration, ecart);
				writerEcart.println(String.format("%d %.20f", iteration, ecart));
                if (ecart < e)
                    break;
            }
            
            prevPiK= piK.clone();
        }
        writerEcart.close();
        long fin = System.currentTimeMillis();

        System.out.println("");
        //A.affiche();
        //Matrix.affiche("beta", beta);
       // Matrix.affiche("piK", piK);
		
		double[] piKOrganise = Matrix.reorganise(piK, M.map);
		
        Matrix.affiche("piK organise", piKOrganise);
		
		
		
        System.out.println("Temps exec p + boucle: " + (fin - debut)/(double) 1000);
		System.out.println("Temps contruction de p: " + (fin1 - debut1)/(double) 1000);

        //affichePi();
		File f2 = new File("results/vecteur" + fileName);
		if (f.exists()) {
			f2.delete();
		}
		f.createNewFile();
        PrintStream writerVecteur = new PrintStream(f2);
		
		for(int i = 0 ; i < piKOrganise.length ; i++) {
		writerVecteur.println(String.format("%d %.20f", i, piKOrganise[i]));
		}
		
		writerVecteur.close();
		

    }
}
