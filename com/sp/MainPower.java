package com.sp;

import java.io.File;
import java.io.PrintStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import utils.Matrix;
import utils.InMemorySparseMatrix;
import utils.FileBasedSparseMatrix;
import utils.PartitionResult;

// ********************************************************************************************************************
// Description des variables globales
//     M         represente la matrice de depart dont on doit appliquer l'approche KMS simplifiee
//     G         represente la matrice resultante du partitionnement en composants connexes de M 
//     P         rassemble toutes les matrices Pij
//     partition (nous cherchons encore un nom qui soit plus representatif de cette variable)
//               cette variable nous donne la taille des differents composants connexes de G
//               Ainsi partition = [3, 2] signifie que la partition est constituee de deux composants connexes
//               dont le premier composant est de taille 3, le deuxieme composant est de taille 2
//     A         represente la matrice de couplage
//     beta      represente le vecteur de probabilites (solution de beta x A = beta) obtenu par la methode des puissances
//     alfaK
//     piK
//
// Les matrices M, G et A sont representees par des matrices creuses dont le contenu est persiste sur disque.
//      Meme creuses, elles sont énormes pour que leur contenu puisse etre gardee en mémoire RAM (heap java).
//      C'est pourquoi, leur contenu sont sur disque. N'est gardée en mémoire les cellules le temps d'un calcul.
//      Elles sont de classe FileBasedSparseMatrix
// Les matrices Pij sont également de matrices creuse.
//      Pour l'instant, on part de l'hypothese que leur contenu puisse etre entirement en mémoire RAM (heap java)
//      Elles sont de classe InMemorySparseMatrix.
//      Si par la suite, l'hypothèse s'avère fausse, on les declarera comme classe FileBasedSparseMatrix.
//
// pseudo code
//     remplir la matrice M a partir d'un fichier                               -> M
//     partitionne la matrice M selon les composants connexes                   -> G, partition
//     initialiser alfaK
//     initialiser piK
//     a partir de G construire P (autrement dit, on construit tous les Pij)    -> P
//     initialiser pi
//     on boucle jusqu'à convergence
//          en fonction de pi calculer la matrice de couplage                   -> A
//          calculer vecteur de probabilite de A                                -> beta
//          calculer alfaK                                                      -> alfaK
//          calculer piK                                                        -> piK
//          calculer pi
//          comparer piK avec sa precedente valeur comme test de convergence
// ********************************************************************************************************************
public class MainPower {
    static FileBasedSparseMatrix M;
    
    static double[] f;                              // f associee à la matrice G
                       
                           
    // *************************************
    //      pi = (pi1, pi2)
    // *************************************
    static double[][] pi;

    

    // *************************************
    //        vecteur de probabilite
    // *************************************
    
    // **************************************************************************
    // affiche Pi
    // --------------------------------------------------------------------------
    // Trop d'informations tue l'information
    // On n'affiche que les 10 premieres valeurs de pi 
    // **************************************************************************
    

    // ***********************************************************************************************************
    // On utilise la methode des puissances pour calculer le vecteur de probabilites
    // En entree:
    //     les variables globaux 
    //         double[][] A
    //         double[] partition
    // En sortie
    //     retourne le vecteur de probabilité    
    // A Faire:
    //     Il faut permettre de variabiliser 
    //         alfa
    //         iteration
    // ***********************************************************************************************************
    public static double[] calculVecteurDeProbabilite(double alfa, double[] f, double e) throws IOException {
//      System.out.println("    calculVecteurDeProbabilite()");
        int n= M.nbLignes;;
        double[] p= new double[n];
        for (int i=0; i<n; i++)
            p[i]= ((double)1)/n;
        double correction2= ((double)1-alfa)/n;
        
        double[] prevP= p;
//      int since= 0;
//      long t1= System.currentTimeMillis();
        for (int iteration=1; ; iteration++) {
            p= M.mult(prevP);           
            double correction1= Matrix.mult(prevP, f)/n;
            for (int i=0; i<n; i++) {
                p[i]= alfa*p[i]+correction1+correction2;
            }
            double ecart=0;
            for (int i=0; i<n; i++) {
                double delta= Math.abs(p[i]-prevP[i]);
                ecart= Math.max(ecart, delta);
            }
            if (ecart<=e) {
                break;
            }
            System.out.printf("\t\t\t%8d\t%.11f\n",iteration, ecart);

//*/
            prevP= p;
        }

        return p;
    }    
    
    public static void calculF() {
        int n = M.nbLignes;
        f= new double[n];
        for (int i=0; i<n; i++) {
            f[i]= 1;
        }
        M.forEach((r, c, v) -> {
            if (v==0)
                return;
            f[r]=0;
        }
);
    }
    public static void main(String[] args) throws IOException {
        double alfa= 0.95;
        double e=10E-10;
        
        String fileName= "wb-edu.txt";
        if (args.length==1)
            fileName= args[0];
        
        System.out.printf("fileName= '%s'\n", fileName);
    
        
        M= new FileBasedSparseMatrix("M");
        M.lire("data/" + fileName);
        M.affiche();
        
        long debut = System.currentTimeMillis();
        
        calculF();

        double[] p =  calculVecteurDeProbabilite(alfa, f, e);
        
        long fin = System.currentTimeMillis();

		
    	PrintStream writer = new PrintStream(new File("references/" + fileName));
		for(int i = 0 ; i < p.length ; i++) {
			writer.println(String.format("%d %.20f", i, p[i]));
		}
        writer.close();    
        System.out.println("");
        //A.affiche();
        Matrix.affiche("p", p);
        System.out.println("Temps : " + (fin - debut)/(double) 1000);
    }
}
